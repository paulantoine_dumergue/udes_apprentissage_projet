# -*- coding: utf-8 -*-
from sklearn.decomposition import PCA
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.preprocessing import LabelEncoder, StandardScaler


class Data:
    def __init__(self, training_data, test_data):
        """
        Constructor
        Encode data and apply PCA
        :param training_data: raw training data from csv
        :param test_data: raw training data from csv
        :return:

        """
        self.x_train, self.y_train, self.classes = Data.encode_training_data(training_data)
        self.test_ids, self.x_test = Data.encode_test_data(test_data)

        # We apply PCA to reduce dimensions
        pca = PCA(n_components=0.999999, svd_solver='full')
        pca.fit(self.x_train)
        self.x_train = pca.transform(self.x_train)
        self.x_test = pca.transform(self.x_test)
        # print(len(pca.explained_variance_ratio_), sum(pca.explained_variance_ratio_), pca.explained_variance_ratio_)

    @staticmethod
    def encode_training_data(training_data):
        """
        Separate training data into matrix x and vector y
        :param training_data: raw data from csv
        :return: encoded training data
        """
        x_train = training_data.drop(['id', 'species'], axis=1).values

        # encoding the name of the plant species
        label_encoder = LabelEncoder().fit(training_data['species'])
        y_train = label_encoder.transform(training_data['species'])
        classes = label_encoder.classes_

        # standerization of the features
        scaler = StandardScaler().fit(x_train)
        x_train = scaler.transform(x_train)
        return x_train, y_train, classes

    @staticmethod
    def encode_test_data(test_data):
        """
        Parse testing data into matrix x
        :param test_data: raw data from csv
        :return: encoded testing data
        """
        test_ids = test_data.id

        # Drop the id and species
        test_data = test_data.drop(['id'], axis=1)

        # Remove the first row from the training data set and Convert the data into Numpy array
        scaler = StandardScaler().fit(test_data)
        x_test = scaler.transform(test_data)

        return test_ids, x_test

    def shuffle_split(self, training_percentage=0.75):
        """
        Split training data into new training and testing datasets
        :param training_percentage:
        :return:
        """
        x_train, x_test, y_train, y_test = None, None, None, None
        splitter = StratifiedShuffleSplit(n_splits=1, random_state=42, test_size=(1-training_percentage))
        for train_index, test_index in splitter.split(self.x_train, self.y_train):
            x_train, y_train = self.x_train[train_index], self.y_train[train_index]
            x_test, y_test = self.x_train[test_index], self.y_train[test_index]

        return x_train, x_test, y_train, y_test
