import pandas as pd

from model.data import Data


class FileReader:
    """
    FileReader
    Class used for csv reading and Data object instance building
    """

    @staticmethod
    def read_files(train_file, test_file):
        """
        Read the csv and convert it into an object
        :param train_file: path to train file
        :param test_file: path to test file
        :return: Data instance with values from files
        """
        test = pd.read_csv(test_file)
        train = pd.read_csv(train_file)
        return Data(train, test)

